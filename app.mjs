import {createSocket} from "dgram";

const sock = createSocket("udp4", (msg, rinfo) => {
  console.log(rinfo);
  sock.send(JSON.stringify(rinfo), rinfo.port, rinfo.address);
}).bind(8080);
